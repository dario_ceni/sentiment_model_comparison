# importing packages
import seaborn
import matplotlib.pyplot as plt
import pandas as pd
from math import dist


nomeCorrenteDataset = "dataset_21_03_2023"
df = pd.read_csv(f'data/{nomeCorrenteDataset}.csv')

df = df[["feel_it_SCORE", "xml_roberta_SCORE", "vader_SCORE_pnn","vader_SCORE_pn"]]


for i, score in enumerate(df.feel_it_SCORE):
    if score == "positive":
        score = 1
    elif score == "neutral":
        score = 0
    elif score == "negative":
        score = -1

    df.at[i, 'feel_it_SCORE'] = score


for i, score in enumerate(df.xml_roberta_SCORE):
    if score == "positive":
        score = 1
    elif score == "neutral":
        score = 0
    elif score == "negative":
        score = -1

    df.at[i, 'xml_roberta_SCORE'] = score


for i, score in enumerate(df.vader_SCORE_pnn):
    if score == "positive":
        score = 1
    elif score == "neutral":
        score = 0
    elif score == "negative":
        score = -1

    df.at[i, 'vader_SCORE_pnn'] = score

for i, score in enumerate(df.vader_SCORE_pn):
    if score == "positive":
        score = 1
    else:
        score = -1

    df.at[i, 'vader_SCORE_pn'] = score

df.to_csv(f'data/{nomeCorrenteDataset}.csv')