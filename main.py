# importing packages
import seaborn
import matplotlib.pyplot as plt
import pandas as pd
from math import dist


nomeCorrenteDataset = "dataset_21_03_2023"
df = pd.read_csv(f'data/{nomeCorrenteDataset}.csv')

df = df[["feel_it_SCORE", "xml_roberta_SCORE", "vader_SCORE_pnn","vader_SCORE_pn"]]


#seaborn.pairplot(df)
#plt.show()
confronto_pnn = [0,0,0]

for i, score in enumerate(df.vader_SCORE_pnn):
    b = df.at[i, 'xml_roberta_SCORE']
    #print(f"vader: {score}, roberta: {b},{dist([score], [b])}")
    confronto_pnn[int(dist([score], [b]))] += 1
    distanza = int(dist([score], [b]))

print(confronto_pnn[0], confronto_pnn[1], confronto_pnn[2])

confronto_pn = [0,0]

for i, score in enumerate(df.vader_SCORE_pn):
    b = df.at[i, 'feel_it_SCORE']
    #print(f"vader: {score}, roberta: {b},{dist([score], [b])}")
    confronto_pn[int(dist([score], [b]) / 2)] += 1
    distanza = int(dist([score], [b]))

print(confronto_pn[0], confronto_pn[1])

seaborn.pairplot(df)
plt.show()